package resources;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import io.netty.handler.codec.http.router.Router;

public class ReadRouter {
	Router<String> router = new Router<String>();
	InputStream inputStream;

	public Router<String> getRouter() throws IOException {
		try {
			Properties prop = new Properties();
			String propFileName = "router.properties";

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			String value="";
			String [] value_arr;
		    for(int i = 0; (value = prop.getProperty("router." + i)) != null; i++) {
		    	value_arr = value.split(",");
		    	switch(value_arr[0]) {
			    	case "GET": router.GET(value_arr[1], value_arr[2]); break;
			    	case "POST": router.POST(value_arr[1], value_arr[2]); break;
			    	case "PUT": router.PUT(value_arr[1], value_arr[2]); break;
			    	case "DELETE": router.DELETE(value_arr[1], value_arr[2]); break;
			    	default:break;
		    	}
		    	
		    }

		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return router;
	}
}
