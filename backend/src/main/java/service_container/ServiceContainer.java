package service_container;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import command.Command;
import command_provider.CommandProvider;

public class ServiceContainer {
	private static final int DEFAULT_HANDLER_COUNT = 100;
	private static final int DEFAULT_PREFETCH_COUNT = 1;
	private String RPC_QUEUE_NAME;
	private ExecutorService executor;
	private int channelCount;
	private CommandProvider commandProvider;
	private int prefetchCount;
	

	public ServiceContainer(String RPC_QUEUE_NAME, CommandProvider commandProvider, ExecutorService executor,
			int channelCount, int prefetchCount) {
		this.RPC_QUEUE_NAME = RPC_QUEUE_NAME;
		this.commandProvider = commandProvider;
		this.executor = executor;
		this.channelCount = channelCount;
		this.prefetchCount = prefetchCount;
		this.initialize();
	}
	
	public ServiceContainer(String RPC_QUEUE_NAME, CommandProvider commandProvider) {
		this(RPC_QUEUE_NAME, commandProvider, 
				Executors.newFixedThreadPool(DEFAULT_HANDLER_COUNT), DEFAULT_HANDLER_COUNT, DEFAULT_PREFETCH_COUNT);
	}


	public void initialize() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setSharedExecutor(this.executor);
		factory.useNio();
		factory.setHost("localhost");

		Connection connection = null;
		try {
			connection = factory.newConnection();
			System.out.println(" [x] Awaiting RPC requests");
			for(int i = 0; i < this.channelCount; i++) {
				final Channel channel = connection.createChannel();
				channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
				channel.basicQos(this.prefetchCount);
				
				Consumer consumer = new DefaultConsumer(channel) {
					@Override
					public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
							byte[] body) throws IOException {
						AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder()
								.correlationId(properties.getCorrelationId()).build();
						System.out.println("Responding to corrID: " + properties.getCorrelationId());

						JSONObject body_json = new JSONObject(new String(body, "UTF-8"));
						System.out.println(body_json);
						JSONObject response = new JSONObject();

						try {
							Command command = commandProvider.getCommandInstance(body_json.getString("uri"));
							if(command == null) {
								response = new JSONObject();
								response.put("error", "unsupported action");
							}
							else
								response = command.run(body_json);
						} catch (RuntimeException | InstantiationException | IllegalAccessException e) {
							System.out.println(" [.] " + e.toString());
						} finally {
							channel.basicPublish("", properties.getReplyTo(), replyProps,
									response.toString().getBytes("UTF-8"));
							channel.basicAck(envelope.getDeliveryTag(), false);
						}
					}
				};

				channel.basicConsume(RPC_QUEUE_NAME, false, consumer);
			}

			try {
				synchronized(this) {
					this.wait();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		} finally {
			if (connection != null)
				try {
					connection.close();
				} catch (IOException _ignore) {}
		}
	}
}
