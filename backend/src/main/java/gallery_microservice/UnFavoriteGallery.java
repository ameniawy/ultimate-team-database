package gallery_microservice;

import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class UnFavoriteGallery implements Command {
	
	PostgresService db;
	
	public UnFavoriteGallery() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		res.put("status", "200");
		try {
			String resMsg = "Sucessfully removed gallery with ID: " + request.getString("gallery_id")+ " from favorites of the user with ID: " + request.getString("user_id");
			JSONObject sqlObj = unFavoriteGallery(request);
			if (sqlObj != null) {
				resBody.put("user_msg", resMsg);
				resBody.put("result", sqlObj);
			} else {
				resBody.put("user_msg", "Failed to unfavorite gallery.");
				resBody.put("error", "Database not updated.");
				res.put("status", "400");
			}
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to unfavorite gallery.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}
	
	public JSONObject unFavoriteGallery(JSONObject request) throws Exception {
		try {
			String user_id;
			String gallery_id = request.getString("gallery_id");
			try {
				user_id = request.getString("user_id");
				String sql = "select unfavorite_gallery ('%s', '%s')";
				String resSQL = String.format(sql, user_id, gallery_id);
				System.out.println(resSQL);
				return db.execute(resSQL);
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
	}
}
