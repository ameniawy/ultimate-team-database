package gallery_microservice;

import org.json.JSONArray;
import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;


public class SearchTitle implements Command {
	
	PostgresService db;
	
	public SearchTitle() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		res.put("status", "200");
		try {
			JSONArray temp = searchGallery(request);
			if (temp!=null) {
				resBody.put("user_msg", "Fetched search results.");
				resBody.put("result", temp);
			} else {
				resBody.put("user_msg", "Failed to search search.");
				resBody.put("error", "Database not updated.");
				res.put("status", "400");
			}
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to fetch search.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}
	
	public JSONArray searchGallery(JSONObject request) throws Exception {
		JSONArray q = new JSONArray();
		try {
			String name = request.getString("name");
			String sql = "select search_galleries ('%s')";
			String resSQL = String.format(sql, name);
			System.out.println(db.execute(resSQL));
			q = db.execute(resSQL).getJSONArray("Result");
			System.out.println(q);
			return q;
		} catch (Exception e) {
			throw e;
		}
	}
}
