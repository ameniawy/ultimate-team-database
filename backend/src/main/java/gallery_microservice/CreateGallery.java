package gallery_microservice;

import sql_microservice.PostgresService;
import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;

public class CreateGallery implements Command {
	
	private PostgresService db;
	
	public CreateGallery() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		res.put("status", "200");
		String resMsg = "Sucessfully created gallary for ID: " + request.getString("user_id");
		try {
			JSONObject sqlObj = createGallery(request);
			if (sqlObj != null) {
				resBody.put("user_msg", resMsg);
				resBody.put("result", sqlObj);
			} else {
				resBody.put("user_msg", "Failed to create gallery.");
				resBody.put("error", "Database not updated.");
				res.put("status", "400");
			}
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to create gallery.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");	
		}
		res.put("message", resBody);
		return res;
	}
	
	public JSONObject createGallery(JSONObject request) throws Exception {
		try {
			String name = request.getString("name");
			String description = request.getString("description");
			String user_id = request.getString("user_id");
			
			String sql = "select create_gallery ('%s', '%s', '%s')";
			String resSQL = String.format(sql, user_id, name, description);
			System.out.println(resSQL);
			return db.execute(resSQL);
		} catch (Exception e) {
			throw e;
		}
	}
}
