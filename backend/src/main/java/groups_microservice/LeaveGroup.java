package groups_microservice;

import org.json.JSONObject;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class LeaveGroup implements Command {
	
	private PostgresService db;
	
	public LeaveGroup() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		//Authentication Still needed
		JSONObject user;
		String groupID;
		JSONObject res = new JSONObject();
		try {
			user = (JSONObject) request.get("user");
			groupID = request.getString("group_id");
		}catch(Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "400");
			return res;
		}
		String userID = user.getString("_id");
		String sql = "select delete_user_group('%s', '%s')";
		String resSQL = String.format(sql, userID, groupID);
		try {
			db.execute(resSQL);
			res.put("message", new JSONObject().put("result", "Left Group"));
			res.put("status", "200");
			return res;
		} catch (Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "500");
			return res;
		}
	}
}
