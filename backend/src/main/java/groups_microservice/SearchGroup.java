package groups_microservice;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class SearchGroup {
	
	private PostgresService db;
	private ArangoService arango;
	
	public SearchGroup() {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		db = serviceLocator.getServiceByName("pg");
		this.arango = serviceLocator.getServiceByName("arango");
	}

	public JSONObject run(JSONObject request) 
	{
		JSONObject groups = new JSONObject();
		JSONObject res = new JSONObject();
		try {
			//Authentication Still needed
			String search = (String) request.get("search");
			JSONArray groupsArr = new JSONArray();
			ArrayList<String> groups_AL = new ArrayList<String>();
			ArrayList<JSONObject> groups_AL_JSON = new ArrayList<JSONObject>();
			
			//search for groups by owner select get_user(1)
			String sqlStr = "select * from get_user_by_name('%s')";
			JSONObject users = db.execute(String.format(sqlStr,search));
			JSONArray users_array = (JSONArray) users.get("Result");
			List<String> user_ids  = getValuesForGivenKey(users_array.toString(),"id");
			
			String owner_query = "FOR t IN Groups FILTER t.admin_id == @admin_id RETURN t";
			Map<String, Object> bindvars= new HashMap<String, Object>();
	
			for(int j = 0; j < user_ids.size(); j++) 
			{
				bindvars.put("admin_id", user_ids.get(j));
				JSONObject user_groups = arango.query(owner_query, bindvars);
				JSONArray user_groups_json_array = (JSONArray)user_groups.get("result");
				for(int i=0 ; i < user_groups_json_array.length();i++) 
				{
					groups_AL_JSON.add(new JSONObject(user_groups_json_array.get(i).toString()));
				}	
			}
			
			//search for groups by title
			String title_query = "FOR t IN Groups FILTER t.title LIKE @title RETURN t";		
			Map<String, Object> bindvars_1= new HashMap<String, Object>();
			
			bindvars_1.put("title",search);
			JSONObject title_groups = arango.query(title_query, bindvars_1);
			JSONArray title_groups_json_array = (JSONArray) title_groups.get("result");
			
			for(int i = 0 ; i<(title_groups_json_array).length(); i++)
			{
				groups_AL_JSON.add(new JSONObject(title_groups_json_array.get(i).toString()));
			}
			
			ArrayList<String> ids = (ArrayList<String>) groups_AL_JSON.stream().map(j -> j.getString("id")).distinct().collect(Collectors.toList()); 
			groups_AL_JSON = (ArrayList<JSONObject>) groups_AL_JSON.stream().filter(j -> ids.contains(j.getString("id")) && ids.remove(j.getString("id"))).collect(Collectors.toList());
			groups.put("result", groups_AL_JSON);
			res.put("message", groups);
			res.put("status", "200");
			return res;
		} catch (Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "500");
			return res; 
		}
	}
	
	//returns array of specific key values of json string				
	private List<String> getValuesForGivenKey(String jsonArrayStr, String key) {
	    JSONArray jsonArray = new JSONArray(jsonArrayStr);
	    return IntStream.range(0, jsonArray.length())
	      .mapToObj(index -> ((JSONObject)jsonArray.get(index)).optString(key))
	      .collect(Collectors.toList());
	}
}
