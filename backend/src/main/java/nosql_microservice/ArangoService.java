package nosql_microservice;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDB;
import com.arangodb.ArangoDBException;
import com.arangodb.ArangoDatabase;
import com.arangodb.entity.BaseDocument;
import com.arangodb.entity.CollectionEntity;
import com.arangodb.entity.DocumentCreateEntity;
import com.arangodb.model.CollectionCreateOptions;
import com.arangodb.util.MapBuilder;

public class ArangoService {

	private ArangoDB arangoDB;
	private ArangoDatabase db;
	private final String DB_NAME;
	private String host;
	private int port;

	public ArangoService(String host, int port, String dbName) {
		this.DB_NAME = dbName;
		this.host = host;
		this.port = port;
		this.init();
	}

	public void init() {
		arangoDB = new ArangoDB.Builder().host(host, port).build();
		if (!arangoDB.getDatabases().contains(DB_NAME)) {
			createDB();
		}
		db = arangoDB.db(DB_NAME);
		System.out.println(db);
	}

	private void createDB() {
		try {
			arangoDB.createDatabase(DB_NAME);
			System.out.println("Database created: " + DB_NAME);
		} catch (ArangoDBException e) {
			System.err.println("Failed to create database: " + DB_NAME + "; " + e.getMessage());
		}
	}

	public void dropDB() {
		arangoDB.db(DB_NAME).drop();
	}

	public void createCollection(String collectionName) {
		try {
			CollectionEntity myArangoCollection = db.createCollection(collectionName);
			System.out.println("Collection created: " + myArangoCollection.getName());
		} catch (ArangoDBException e) {
			System.err.println("Failed to create collection: " + collectionName + "; " + e.getMessage());
		}
	}

	public String createDocument(String collectionName, Object myObject) {
		try {
			System.out.println(myObject.toString());
			DocumentCreateEntity<Object> info = db.collection(collectionName).insertDocument(myObject);
			System.out.println("Document created");
			return info.getKey();
		} catch (ArangoDBException e) {
			System.err.println("Failed to create document. " + e.getMessage());
			db.createCollection(collectionName, new CollectionCreateOptions());
			try {
				DocumentCreateEntity<Object> info = db.collection(collectionName).insertDocument(myObject);
				System.out.println("Document created");
				return info.getKey();
			} catch (ArangoDBException e2) {
				System.err.println("Failed to create document. again " + e2.getMessage());
				return e2.getErrorMessage();
			}
		}
	}
	

	public JSONObject readDocument(String collectionName, String key) {
		JSONObject json = new JSONObject();
		JSONObject obj = null;
		try {
			BaseDocument myDocument = db.collection(collectionName).getDocument(key, BaseDocument.class);
			obj = new JSONObject();
			obj.put("id", myDocument.getKey());
			for (String k : myDocument.getProperties().keySet()) {
				obj.put(k, myDocument.getProperties().get(k));
			}

		} catch (ArangoDBException e) {
			System.err.println("Failed to get document: myKey; " + e.getMessage());
		} catch (NullPointerException e) {
			System.err.println("Failed to get document: myKey; " + e.getMessage());
		}
		json.put("result", obj);
		return json;
	}

	public void updateDocument(String collectionName, String key, BaseDocument myObject) {
		try {
			db.collection(collectionName).updateDocument(key, myObject);
		} catch (ArangoDBException e) {
			System.err.println("Failed to update document. " + e.getMessage());
		}
	}

	public void deleteDocument(String collectionName, String key) {
		try {
			db.collection(collectionName).deleteDocument(key);
		} catch (ArangoDBException e) {
			System.err.println("Failed to delete document. " + e.getMessage());
		}
	}

	public JSONObject query(String query, Map<String, Object> properties) {

		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();

		MapBuilder mapbuilder = new MapBuilder();

		for (String key : properties.keySet()) {
			mapbuilder.put(key, properties.get(key));
		}

		ArangoCursor<BaseDocument> cursor = null;
		try {
			// the AQL query uses the placeholder @name which has to be bind to a value
			cursor = db.query(query, mapbuilder.get(), null, BaseDocument.class);
			cursor.forEachRemaining(aDocument -> {
				JSONObject obj = new JSONObject();
				obj.put("id", aDocument.getKey());
				for (String key : aDocument.getProperties().keySet()) {
					obj.put(key, aDocument.getProperties().get(key));
				}
				jsonArray.put(obj);
			});

		} catch (ArangoDBException e) {
			System.err.println("Failed to execute query. " + e.getMessage());
		}
		json.put("result", jsonArray);
		return json;
	}
	public JSONObject deleteDocument(String query, Map<String, Object> properties) {
		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();

		MapBuilder mapbuilder = new MapBuilder();

		for (String key : properties.keySet()) {
			mapbuilder.put(key, properties.get(key));
		}

		ArangoCursor<BaseDocument> cursor = null;
		try {
			//ArangoService.init();
			cursor = db.query(query, mapbuilder.get(), null,
					BaseDocument.class);
			cursor.forEachRemaining(aDocument -> {
				jsonArray.put(aDocument.getKey());
				System.out.println("Removed document " + aDocument.getKey());
			});
		} catch (ArangoDBException e) {
			System.err.println("Failed to execute query. " + e.getMessage());
		}
		json.put("deleted_posts", jsonArray);
		return json;
	}
}
