package users_microservice;

import org.json.JSONObject;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class DeleteUser implements Command {
	private PostgresService db;

	public DeleteUser() {
		this.db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();

		try {
			res.put("status", "200");
			String resMsg = "Sucessfully deleted User with ID: " + request.getString("deleted_id");
			JSONObject sqlObj = deleteUser(request);

			resBody.put("user_msg", resMsg);
			resBody.put("user", sqlObj);
			// TODO: check for updated rows
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to edit user.");
			resBody.put("error", e.getMessage());
		}
		res.put("message", resBody);
		return res;
	}

	public JSONObject deleteUser(JSONObject request) throws Exception {
		try {
			String otherUserID = request.getString("deleted_id");

			String sql = "select delete_user ('%s')";
			String resSQL = String.format(sql, otherUserID);
			System.out.println(resSQL);
			return db.execute(resSQL);
		} catch (Exception e) {
			throw e;
		}
	}

}
