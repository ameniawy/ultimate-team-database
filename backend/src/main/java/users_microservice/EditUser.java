package users_microservice;

import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class EditUser implements Command {
	private PostgresService db;

	public EditUser() {
		this.db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		try {
			res.put("status", "200");
			String resMsg = "Sucessfully updated user.";
			JSONObject sqlObj = editUser(request);
			resBody.put("user_msg", resMsg);
			resBody.put("user", sqlObj);
			// TODO: check for updated rows
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to edit user.");
			resBody.put("error", e.getMessage());
		}
		res.put("message", resBody);
		return res;
	}

	public String hashPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}

	public JSONObject editUser(JSONObject request) throws Exception {

		try {
			String id = request.getString("id");
			String name = request.getString("name");
			String birthDate = request.getString("birthdate");
			String mobileNumber = request.getString("mobile_number");
			String gender = request.getString("gender");
			String coverPic = request.getString("cover_pic");
			String profilePic = request.getString("profile_pic");

			String sql = "select update_user ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
			String resSQL = String.format(sql, id, name, birthDate, mobileNumber, gender, coverPic, profilePic);
			System.out.println(resSQL);
			return db.execute(resSQL);
		} catch (Exception e) {
			throw e;
		}
	}

}
