package users_microservice;

import java.security.Key;

import org.json.JSONObject;

import configuration.AppConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import service_locator.ServiceLocator;

public class JWTService {
	private String privateKey;

	public JWTService() {
		AppConfig appConfig = ServiceLocator.getInstance().getServiceByName("config");
		this.privateKey = appConfig.getConfigVar("privateKey");
	}

	public String encode(String payload) {
		byte[] keyBytes = this.privateKey.getBytes();
		Key key = Keys.hmacShaKeyFor(keyBytes);
		String jws = Jwts.builder().setSubject(payload).signWith(key).compact();
		return jws;
	}

	public JSONObject decode(String jws) {
		byte[] keyBytes = this.privateKey.getBytes();
		Key key = Keys.hmacShaKeyFor(keyBytes);
		Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(jws);
		return new JSONObject(claims.getBody().getSubject());
	}
}
