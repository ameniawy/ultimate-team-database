package sql_microservice;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import com.github.jasync.sql.db.Configuration;
import com.github.jasync.sql.db.ConnectionPoolConfiguration;
import com.github.jasync.sql.db.ConnectionPoolConfigurationBuilder;
import com.github.jasync.sql.db.QueryResult;
import com.github.jasync.sql.db.pool.ConnectionPool;
import com.github.jasync.sql.db.postgresql.PostgreSQLConnection;
import com.github.jasync.sql.db.postgresql.pool.PostgreSQLConnectionFactory;

import rx.annotations.Experimental;

@Experimental
public class AsyncPostgresService {
	private Configuration dbConfig;
	private ConnectionPoolConfiguration poolConfig;
	private ConnectionPool<PostgreSQLConnection> dbConnection;
	
	public AsyncPostgresService(AsyncPostgresServiceBuilder builder) {
		dbConfig = new Configuration(builder.username, builder.url, builder.port, builder.password, builder.dbName);
		ConnectionPoolConfigurationBuilder poolBuilder = new ConnectionPoolConfigurationBuilder();
		poolBuilder.setMaxActiveConnections(builder.maxNumberOfConnections);
		poolBuilder.setMaxIdleTime(builder.maxIdleConnectionTime);	
		poolBuilder.setMaxPendingQueries(builder.maxQueryQueueSize);
		poolBuilder.setExecutionContext(builder.executor);
		poolConfig = poolBuilder.build();
		dbConnection = new ConnectionPool<PostgreSQLConnection>(new PostgreSQLConnectionFactory(dbConfig), poolConfig);
	}

	public void connect(int timeout) throws InterruptedException, ExecutionException, TimeoutException {
		this.dbConnection.connect().get(timeout, TimeUnit.MILLISECONDS);
	}
	
	public CompletableFuture<QueryResult> executeStatementWithParams(String query, List<? extends Object> params)
	{
		return dbConnection.sendPreparedStatement(query, params);
	}
	
	public void closeConnection() {
		this.dbConnection.disconnect();
	}
	
	public static class AsyncPostgresServiceBuilder {
		private String dbName;
		private String url;
		private int port;
		private String username;
		private String password;
		private int maxNumberOfConnections;
		private int maxQueryQueueSize;
		private long maxIdleConnectionTime;
		private ExecutorService executor;
		
		public AsyncPostgresServiceBuilder setDBName(String dbName) {
			this.dbName = dbName;
			return this;
		}
		
		public AsyncPostgresServiceBuilder setUrl(String url) {
			this.url = url;
			return this;
		}

		public AsyncPostgresServiceBuilder setPort(int port) {
			this.port = port;
			return this;
		}

		public AsyncPostgresServiceBuilder setUsername(String username) {
			this.username = username;
			return this;
		}

		public AsyncPostgresServiceBuilder setPassword(String password) {
			this.password = password;
			return this;
		}

		public AsyncPostgresServiceBuilder setMaxNumberOfConnections(int maxNumberOfConnections) {
			this.maxNumberOfConnections = maxNumberOfConnections;
			return this;
		}

		public AsyncPostgresServiceBuilder setMaxQueryQueueSize(int maxQueryQueueSize) {
			this.maxQueryQueueSize = maxQueryQueueSize;
			return this;
		}

		public AsyncPostgresServiceBuilder setMaxIdleConnectionTime(long maxIdleConnectionTime) {
			this.maxIdleConnectionTime = maxIdleConnectionTime;
			return this;
		}
		
		public AsyncPostgresServiceBuilder setExecutorService(ExecutorService executor) {
			this.executor = executor;
			return this;
		}
		
		public AsyncPostgresService build() {
			return new AsyncPostgresService(this);
		}
	}
	
}
