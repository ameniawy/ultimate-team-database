package comment_microservice.entities;

import java.util.Date;

import org.json.JSONObject;

import exceptions.ObjectConstructionError;

public class Comment {
	private int id;
	private int userId;
	private String text;
	private boolean isDeleted;
	private Date createdAt;
	private Date updatedAt;
	private CommentType type;
	private int parentId;
	
	public Comment(int id) {
		this.id = id;
	}
	
	private Comment(CommentBuilder commentBuilder) {
		this(commentBuilder.id);
		this.userId = commentBuilder.userId;
		this.text = commentBuilder.text;
		this.isDeleted = commentBuilder.isDeleted;
		this.createdAt = commentBuilder.createdAt;
		this.updatedAt = commentBuilder.updatedAt;
		this.type = commentBuilder.type;
		this.parentId = commentBuilder.parentId;
	}

	public int getId() {
		return this.id;
	}
	
	public CommentType getType() {
		return this.type;
	}
	
	public int getUserId() {
		return userId;
	}

	public String getText() {
		return text;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	public int getParentId() {
		return this.parentId;
	}

	
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		
		json.put("id", this.id);
		json.put("userId", this.userId);
		json.put("text", this.text);
		json.put("isDeleted", this.isDeleted);
		json.put("createdAt", this.createdAt);
		json.put("updatedAt", this.updatedAt);
		json.put("type", this.type);
		json.put("parentId", this.parentId);
		
		return json;
	}

	public static class CommentBuilder {
		private int id;
		private int userId;
		private String text;
		private boolean isDeleted;
		private Date createdAt;
		private Date updatedAt;
		private CommentType type;
		private int parentId;
		private boolean parentInstantiated;
		
		public CommentBuilder setId(int id) {
			this.id = id;
			return this;
		}
		
		public CommentBuilder setUserId(int userId) {
			this.userId = userId;
			return this;
		}
		
		public CommentBuilder setText(String text) {
			this.text = text;
			return this;
		}
		
		public CommentBuilder setIsDeleted(boolean deleted) {
			this.isDeleted = deleted;
			return this;
		}
		
		public CommentBuilder setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
			return this;
		}
		
		public CommentBuilder setUpdatedAt(Date updatedAt) {
			this.updatedAt = updatedAt;
			return this;
		}
		
		public CommentBuilder setType(CommentType type) {
			this.type = type;
			return this;
		}
		
		public CommentBuilder setParentId(int id) {
			this.parentInstantiated = true;
			this.parentId = id;
			return this;
		}
		
		public Comment build() throws ObjectConstructionError {
			if(this.type == null)
				throw new ObjectConstructionError("Can not construct a comment without a type");
			if(!parentInstantiated)
				throw new ObjectConstructionError("Can not construct a comment without a parent");
			return new Comment(this);
		}
	}
}
