package comment_microservice;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;
import command_provider.CommandProvider;
import comment_microservice.database_acess_layer.CommentAccessor;
import configuration.AppConfig;
import configuration.Configurations;
import exceptions.ServiceStorageException;
import service_container.ServiceContainer;
import service_locator.ServiceLocator;

public class CommentService extends ServiceContainer
{
	public CommentService(CommandProvider commandProvider, ExecutorService executor
			, int channelCount, int messagePrefetchCount) {
		super("comment_microservice", commandProvider, executor, channelCount, messagePrefetchCount);
	}
	
	public static void main(String[] args) throws ServiceStorageException, InterruptedException, ExecutionException, TimeoutException, IOException, ClassNotFoundException
	{
		Configurations.defaultConfig();
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		AppConfig appConfig = serviceLocator.getServiceByName("config");
		
		CommentAccessor accessor = new CommentAccessor(
				serviceLocator.getServiceByName("pg"), serviceLocator.getServiceByName("arango"));
		serviceLocator.storeService("comments", accessor);
		
		new CommentService(
				serviceLocator.getServiceByName("commandRegistry"),
				serviceLocator.getServiceByName("executor"),
				Integer.parseInt(appConfig.getConfigVar("NUM_THREADS")),
				Integer.parseInt(appConfig.getConfigVar("MESSAGE_PREFETCH_COUNT"))
		);
	}
}
