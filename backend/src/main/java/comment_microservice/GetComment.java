package comment_microservice;

import org.json.JSONObject;
import command.Command;
import comment_microservice.database_acess_layer.CommentDAO;
import comment_microservice.entities.Comment;
import service_locator.ServiceLocator;

public class GetComment implements Command {
	private CommentDAO comments;
	
	public GetComment() {
		this.comments = ServiceLocator.getInstance().getServiceByName("comments");
	}
	
	@Override
	public JSONObject run(JSONObject object) {
		JSONObject response = new JSONObject();
		try {
			int id = object.getInt("id");
			Comment comment = comments.getCommentById(id);
			response.put("status", "200");
			response.put("comment", comment.toJson());
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", "400");
			response.put("message", new JSONObject().put("error", "internal server error."));
		}
		return response;
	}
}
