package comment_microservice.exceptions;

import exceptions.ObjectConstructionError;

public class UnknowCommentTypeException extends ObjectConstructionError {
	private static final long serialVersionUID = 1L;

	public UnknowCommentTypeException(String msg) {
		super(msg);
	}

}
