package exceptions;

public class ServiceStorageException extends Exception {
	private static final long serialVersionUID = 1L;

	public ServiceStorageException(String msg) {
		super(msg);
	}
}
