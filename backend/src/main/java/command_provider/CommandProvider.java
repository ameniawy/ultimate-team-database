package command_provider;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;

import command.Command;
import configuration.AppConfig;
import service_locator.ServiceLocator;

public class CommandProvider {
	
	private ConcurrentHashMap<String, Class<? extends Command>> registry;
	private ConcurrentHashMap<String, Command> commandRegistry;
	
	public CommandProvider() {
		this.registry = new ConcurrentHashMap<String, Class<? extends Command>>();
		this.commandRegistry = new ConcurrentHashMap<String, Command>();
	}
	
	public Command getCommandInstance(String name, Object... params) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		if(!this.registry.containsKey(name))
			return null;
		Class<?>[] types = new Class<?>[params.length];
		int i = 0;
		for(Object param : params)
			types[i++] = param.getClass();
		return registry.get(name).getConstructor(types).newInstance(params);
	}
	
	public Command getCommandInstance(String name) throws InstantiationException, IllegalAccessException {
		if(this.commandRegistry.containsKey(name))
			return commandRegistry.get(name);
		else if(this.registry.containsKey(name)) {
			this.commandRegistry.put(name, registry.get(name).newInstance());
			return commandRegistry.get(name);
		}
		return null;
	}
	
	public void updateCommandClass(String name, Class<? extends Command> classImpl) {
		registry.put(name, classImpl);
	}
	
	public void removeCommand(String commandName) {
		this.registry.remove(commandName);
	}

	public static CommandProvider loadFromJSON(JSONObject configVar) throws ClassNotFoundException {
		CommandProvider provider = new CommandProvider();
		for(String key: configVar.keySet()) {
			String className = (String) configVar.get(key);
			AppConfig appConfig = ServiceLocator.getInstance().getServiceByName("config");
			provider.updateCommandClass(key, (Class<? extends Command>) Class.forName(appConfig.getConfigVar("CLASS_PREFIX") + className));
		}
		return provider;
	}
}
