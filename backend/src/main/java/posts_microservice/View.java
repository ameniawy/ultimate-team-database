package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class View implements Command {
	private ArangoService arango;
	
	public View() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        try {
            int postid = object.getInt("id");
            System.out.println(postid);
            Map<String, Object> bindvars = new HashMap<String, Object>();
            bindvars.put("id", postid);
            JSONObject queryRes = arango.query("FOR t IN Posts FILTER t._key == @id RETURN t",bindvars);

            res.put("status", "200");
            res.put("message",new JSONObject().put("result",queryRes));
            System.out.println(queryRes);

        } catch (Exception e) {
//			throw e;
            res.put("status", "500");
            res.put("message",new JSONObject().put("result","error finding post"));
        }
        return res;
    }
}
