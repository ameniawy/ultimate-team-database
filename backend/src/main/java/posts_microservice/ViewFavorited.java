package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONArray;
import org.json.JSONObject;
import sql_microservice.PostgresService;

import java.util.ArrayList;
import java.util.HashMap;

public class ViewFavorited implements Command {
    PostgresService sql_db;
	private ArangoService arango;

    public ViewFavorited() {
    	ServiceLocator serviceLocator = ServiceLocator.getInstance();
    	this.arango = serviceLocator.getServiceByName("arango");
    	this.sql_db = serviceLocator.getServiceByName("pg");
    }

    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        try {
//        	TODO: user procedures
            JSONObject queryRes = sql_db.execute(String.format("SELECT post_id from user_favorite_posts where user_id = '%s'", object.get("user_id")));
            ArrayList<Integer> post_ids = new ArrayList<>();
            for (Object post : (JSONArray)queryRes.get("Result")) {
                post_ids.add( ((JSONObject)post).getInt("post_id"));
            }
            HashMap<String, Object> params = new HashMap<>();
            params.put("post_ids", post_ids);
            JSONObject postsRes = arango.query("FOR p IN Posts FILTER p.id IN @post_ids RETURN p",  params);
            // res.put("posts", postsRes.get("result"));
            res.put("status", "200");
            res.put("message",new JSONObject().put("result", postsRes.get("result")));
        } catch(Exception e) {
            res.put("status", "500");
            res.put("message",new JSONObject().put("error", "Failed to retrieve favorited posts."));
        }
        return res;
    }
}
