package posts_microservice;

import command.Command;
import org.json.JSONObject;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import java.util.HashMap;

public class AddHashtag implements Command {

	private ArangoService arango;
	
	public AddHashtag() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
	
    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        HashMap<String, Object> params = new HashMap<>();
        params.put("user_id", object.get("user_id"));
        params.put("post_id", object.get("post_id"));
        params.put("hashtag", object.get("hashtag"));
        try {
            JSONObject queryRes = arango.query("FOR p IN Posts FILTER p.user_id == @user_id FILTER p.id == @post_id UPDATE p WITH {hashtags: PUSH(p.hashtags, @hashtag)} IN Posts RETURN p", params);
            if (queryRes.length() > 0) {
                res.put("message",new JSONObject().put("result", true));
				res.put("status", "200");
            } else {
				res.put("status", "500");
                res.put("message",new JSONObject().put("error", "Post doesn't exist or you do not have the required permissions to add a hashtag."));
            }
        } catch(Exception e) {
            e.printStackTrace();
            res.put("status", "500");
            res.put("message",new JSONObject().put("error", "Failed to add hashtag to post"));
        }
        return res;
    }
}
