package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONObject;

import java.util.HashMap;

public class Count implements Command {
	private ArangoService arango;
	
	public Count() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
	
    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        try {
            JSONObject queryRes = arango.query("FOR p IN Posts COLLECT post_type = p.type WITH COUNT into num_posts RETURN {post_type, num_posts}", new HashMap<>());
            int num_images = 0, num_videos = 0;
            if (queryRes.has("image") && !queryRes.isNull("image"))
                num_images = queryRes.getInt("image");
            if (queryRes.has("image") && !queryRes.isNull("image"))
                num_videos = queryRes.getInt("video");
            res.put("status", "200");
            res.put("message", new JSONObject().put("result",new JSONObject().put("num_images", num_images).put("num_videos", num_videos)));
            // res.put("num_images", num_images);
            // res.put("num_videos", num_videos);
        } catch(Exception e) {
            res.put("status", "500");
            res.put("message", new JSONObject().put("error", "Failed to retrieve total post count."));
        }
        return res;
    }

    public static void main(String[] args) {
        new Count().run(null);
    }
}
