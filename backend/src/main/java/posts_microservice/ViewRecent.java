package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONObject;

import java.util.HashMap;

public class ViewRecent implements Command {
	private ArangoService arango;
	
	public ViewRecent() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        try {
            JSONObject queryRes = arango.query("FOR p IN Posts FILTER p.created_at >= (DATE_NOW() - 24 * 60 * 60 * 1000) SORT p.created_at DESC RETURN p", new HashMap<>());
            // res.put("posts", queryRes.get("result"));
            res.put("status", "200");
            res.put("message",new JSONObject().put("result", queryRes.get("result")));
        } catch(Exception e) {
            e.printStackTrace();
            res.put("status", "500");
            res.put("message",new JSONObject().put("error", "Failed to retrieve the most recent posts."));
        }
        return res;
    }
}
