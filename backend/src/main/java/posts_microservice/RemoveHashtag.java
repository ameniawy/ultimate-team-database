package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONObject;

import java.util.HashMap;

public class RemoveHashtag implements Command {
	private ArangoService arango;
	
	public RemoveHashtag() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        HashMap<String, Object> params = new HashMap<>();
        params.put("user_id", object.get("user_id"));
        params.put("post_id", object.get("post_id"));
        params.put("hashtag", object.get("hashtag"));
        try {
            JSONObject queryRes = arango.query("FOR p IN Posts FILTER p.user_id == @user_id FILTER p.id == @post_id UPDATE p WITH {hashtags: REMOVE_VALUE(p.hashtags, @hashtag)} IN Posts RETURN p", params);
            if (queryRes.length() > 0) {
                res.put("status", "200");
                res.put("message",new JSONObject().put("result", true));
            } else {
                res.put("status", "500");
                res.put("message",new JSONObject().put("error", "Post doesn't exist or you do not have the required permissions to remove a hashtag."));
            }
        } catch(Exception e) {
            e.printStackTrace();
            res.put("status", "500");
            res.put("message",new JSONObject().put("error", "Failed to remove hashtag from post"));
        }
        System.out.println(res);
        return res;
    }
}
