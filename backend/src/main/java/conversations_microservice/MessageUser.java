package conversations_microservice;

import org.json.JSONObject;

import com.arangodb.entity.BaseDocument;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;
// FIXME: Not finished at all, should move logic to conversations_microservice
public class MessageUser implements Command {
	private PostgresService sql;
	private ArangoService nosql;

	public MessageUser() {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		sql = serviceLocator.getServiceByName("pg");
		nosql = serviceLocator.getServiceByName("arango");
	}

	@Override
	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();

		try {
			res.put("status", "200");
			String resMsg = "Sucessfully created message.";
			JSONObject sqlObj = messageUser(request);
			resBody.put("user_msg", resMsg);
			resBody.put("user", sqlObj);
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to send message.");
			resBody.put("error", e.getMessage());
		}
		res.put("message", resBody);
		return res;
	}
	// TODO:
	// 1) create message (Arango)
	// 2) Add message to conversation
	public JSONObject messageUser(JSONObject request) throws Exception {

		try {
			// TODO: inject auth user id
			int user_id = request.getInt("user_id");
			String conv_id = request.getString("conversation_id");
			String message = request.getString("text");
			String collectionName = "Messages";
			BaseDocument document = new BaseDocument();
			document.addAttribute("text", message);
			document.addAttribute("is_deleted", false);
			document.addAttribute("user_id", user_id);
			document.addAttribute("conversation_id", conv_id);

			String key = nosql.createDocument("Messages", document);

			String query = "select create_message ('%s', '%s', '%s')";
			// TODO: decode user token and get current user id. Message currently has from & to fields with the same ID
			String resSQL = String.format(query, user_id, conv_id, key);
			System.out.println(resSQL);
			return this.sql.execute(resSQL);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

	}

}
