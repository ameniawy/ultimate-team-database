package notifications_microservice;

import org.json.JSONObject;

import command.Command;
import notifications_microservice.database_access_layer.NotificationsDAO;
import notifications_microservice.entities.Notification;
import service_locator.ServiceLocator;

/* TODO: possibly use the cache to store active notifications
 * TODO: this command probably should be usable internally only
 */
public class CreateNotification implements Command {
	private NotificationsDAO notificationsService;
	
	public CreateNotification() {
		this.notificationsService = ServiceLocator.getInstance().getServiceByName("notifications");
	}
	
	@Override
	public JSONObject run(JSONObject object) {
		JSONObject response = new JSONObject();
		try {
			int senderId = object.getInt("user_id");
			int receiverId = object.getInt("receiver_id");
			String senderName = object.getString("sender_name");
			String title = object.getString("title");
			String link = object.getString("link");
			
			Notification notification = (new Notification.NotificationBuilder())
					.setSenderId(senderId)
					.setReceiverId(receiverId)
					.setSenderName(senderName)
					.setTitle(title)
					.setLink(link)
					.build();
			
			int notificationId = notificationsService.createNotification(notification);
			response.put("status", 200);
			response.put("message", new JSONObject().put("id", notificationId));
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", 500);
			response.put("message", new JSONObject().put("error",  "internal server error"));
		}
		return response;
	}

}
