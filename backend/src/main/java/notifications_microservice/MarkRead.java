package notifications_microservice;

import org.json.JSONObject;
import command.Command;
import notifications_microservice.database_access_layer.NotificationsDAO;
import service_locator.ServiceLocator;

public class MarkRead implements Command {
	private NotificationsDAO notificationsService;
	
	public MarkRead() {
		this.notificationsService = ServiceLocator.getInstance().getServiceByName("notifications");
	}
	
	@Override
	public JSONObject run(JSONObject object) {
		JSONObject response = new JSONObject();
		try {
			int userId = object.getInt("user_id");
			int notificationId = object.getInt("notification_id");
			notificationsService.markNotificationAsRead(userId, notificationId);			
			response.put("status", 200);
			response.put("message", new JSONObject().put("result",  true));
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", 500);
			response.put("message", new JSONObject().put("error",  "internal server error"));
		}
		return response;
	}

}
