package notifications_microservice.database_access_layer;

import java.util.Collection;

import notifications_microservice.entities.Notification;

public interface NotificationsDAO {
	int createNotification(Notification notification);
	Collection<Notification> getUserNotifications(int id);
	void markNotificationAsRead(int userId, int id);
	void deleteNotificationById(int userId, int id);
}
