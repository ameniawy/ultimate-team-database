package notifications_microservice.entities;

import java.util.Date;

import org.json.JSONObject;

import exceptions.ObjectConstructionError;

public class Notification {
	private int id;
	private String senderName;
	private int senderId;
	private int receiverId;
	private Date createdAt;
	private Date updatedAt;
	private String title;
	private String link;
	private boolean read;
	
	public Notification(NotificationBuilder notificationBuilder) {
		this.id = notificationBuilder.id;
		this.senderName = notificationBuilder.senderName;
		this.senderId = notificationBuilder.senderId;
		this.receiverId = notificationBuilder.receiverId;
		this.createdAt = notificationBuilder.createdAt;
		this.updatedAt = notificationBuilder.updatedAt;
		this.title = notificationBuilder.title;
		this.link = notificationBuilder.link;
		this.read = notificationBuilder.read;
	}
	
	public int getId() {
		return id;
	}
	public String getSenderName() {
		return senderName;
	}
	public int getSenderId() {
		return senderId;
	}
	public int getReceiverId() {
		return receiverId;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public String getTitle() {
		return title;
	}
	public String getLink() {
		return link;
	}
	public boolean isRead() {
		return read;
	}
	
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		
		json.put("from", (new JSONObject())
				.put("f_user_id", this.senderId)
				.put("f_user_name", this.senderName)
				)
		.put("to", this.receiverId)
		.put("created_time", this.createdAt)
		.put("title", this.title)
		.put("unread", !this.read);
		
		if(id != -1)
			json.put("id", this.id);
		if(this.updatedAt != null)
			json.put("updated_time", this.updatedAt);
		if(this.link != null)
			json.put("link", this.link);
		
		return json;
	}
	
	
	public static class NotificationBuilder
	{
		private int id;
		private boolean idInstantiated;
		private String senderName;
		private int senderId;
		private boolean senderIdInstantiated;
		private int receiverId;
		private boolean recieverIdInstantiated;
		private Date createdAt;
		private Date updatedAt;
		private String title;
		private String link;
		private boolean read;
		
		public NotificationBuilder setId(int id) {
			this.id = id;
			this.idInstantiated = true;
			return this;
		}
		
		public NotificationBuilder setSenderName(String senderName) {
			this.senderName = senderName;
			return this;
		}
		
		public NotificationBuilder setSenderId(int senderId) {
			this.senderId = senderId;
			this.senderIdInstantiated = true;
			return this;
		}
		
		public NotificationBuilder setReceiverId(int receiverId) {
			this.receiverId = receiverId;
			this.recieverIdInstantiated = true;
			return this;
		}
		
		public NotificationBuilder setCreatedAt(Date createAt) {
			this.createdAt = createAt;
			return this;
		}
		
		public NotificationBuilder setUpdatedAt(Date updatedAt) {
			this.updatedAt = updatedAt;
			return this;
		}
		
		public NotificationBuilder setTitle(String title) {
			this.title = title;
			return this;
		}
		
		public NotificationBuilder setLink(String link) {
			this.link = link;
			return this;
		}
		
		public NotificationBuilder setRead(boolean read) {
			this.read = read;
			return this;
		}
		
		public Notification build() throws ObjectConstructionError {
			if(this.senderName == null)
				throw new ObjectConstructionError("can not construct a notification without it's sender name");
			if(!this.senderIdInstantiated)
				throw new ObjectConstructionError("can not construct a notification without it's sender id");
			if(!this.recieverIdInstantiated)
				throw new ObjectConstructionError("can not construct a notification without a receiver id");
			if(this.title == null)
				throw new ObjectConstructionError("can not construct a notification without a title");
			
			if(this.createdAt == null)
				this.createdAt = new Date();
			if(!idInstantiated)
				this.id = -1;
			
			return new Notification(this);
		}
	}
	
}
