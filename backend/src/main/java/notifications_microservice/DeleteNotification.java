package notifications_microservice;

import org.json.JSONObject;

import command.Command;
import rx.annotations.Experimental;

// TODO: check if there is a need for deletion of notifications
@Experimental
public class DeleteNotification implements Command {

	@Override
	public JSONObject run(JSONObject object) {
		JSONObject response = new JSONObject();
		response.put("error", "Not Implemented");
		return response;
	}

}
