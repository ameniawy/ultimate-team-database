package album_microservice;

import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class AddPost implements Command {
	
	private PostgresService db;
	
	public AddPost() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		try {
			
			boolean success = addPost(request);
			
			if (success) {
				
				res.put("status", "200");
				String resMsg = "Sucessfully added post with ID: " + request.getString("post_id")+ " to albums with ID: " + request.getString("album_id");
				resBody.put("user_msg", resMsg);
			} else {
				res.put("status", "400");
				resBody.put("user_msg", "Failed to add post.");
				resBody.put("error", "No rows updated");
			}
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to add post.");
			resBody.put("error", e.getMessage());
			
		}
		res.put("message", resBody);
		return res;
	}
	
	public Boolean addPost(JSONObject request) throws Exception{

		try {
			String post_id = request.getString("post_id");
			String album_id = request.getString("album_id");

			String sql = "select add_post('%s', '%s')";
			String resSQL = String.format(sql, post_id, album_id);
			JSONObject resObj = db.execute(resSQL);

			if (resObj == null) {
				return false;
			}
			System.out.println(resObj);
		} catch (Exception e) {
			throw e;
		}

		return true;
	}

}
