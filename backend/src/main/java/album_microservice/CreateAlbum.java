package album_microservice;

import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

// belbesi-kady
public class CreateAlbum implements Command {
	
	private PostgresService db;
	
	public CreateAlbum() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject album) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		
		try {
			boolean success = createAlbum(album);
			if (success) {
				res.put("status", "200");
				String resMsg ="Sucessfully created album with user ID: " + album.getString("user_id");
				resBody.put("user_msg", resMsg);
				
			} else {
				res.put("status", "400");
				resBody.put("user_msg", "Failed to create album.");
				resBody.put("error", "No rows updated");

			}
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to create album.");
			resBody.put("error", e.getMessage());
		}
		
		res.put("message", resBody);
		return res;	
	}
	
	public Boolean createAlbum(JSONObject album) throws Exception{

		try {
			String name = album.getString("name");
			String description = album.getString("description");
			String user_id = album.getString("user_id");
			
			String sql = "select create_album ('%s', '%s', '%s')";
			String resSQL = String.format(sql, user_id, name, description);
			JSONObject resObj = db.execute(resSQL);

			if (resObj == null) {
				return false;
			}
			System.out.println(resObj);
		} catch (Exception e) {
			throw e;
		}

		return true;
	}

}
