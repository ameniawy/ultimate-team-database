/*
 * A function to be triggered before updating to updated the updated_at field.
 */
CREATE OR REPLACE FUNCTION trigger_set_timestamp()
    RETURNS TRIGGER AS $$
        BEGIN
            NEW.updated_at = NOW();
        RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;

CREATE TRIGGER set_timestamp
    BEFORE UPDATE ON CurrentlyTrending
    FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_timestamp();

/*
 * A procedure to get the TOP first count posts [where count is an integer that reperesents number of posts to be retrieved]. 
 */

CREATE OR REPLACE PROCEDURE get_trending(count integer)
    LANGUAGE SQL
    AS $$
        SELECT TOP count 
        FROM CurrentlyTrending
        ORDER BY index;
    $$;

