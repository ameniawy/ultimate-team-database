CREATE TABLE Reports (
    id INTEGER NOT NULL,
    report_type ENUM ('PHOTO', 'VIDEO', 'GROUP') NOT NULL,
    f_reported_id INTEGER NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    is_deleted BOOLEAN,
    FOREIGN KEY f_user_id INTEGER REFERENCES Users(id),
    CONSTRAINT p_report PRIMARY KEY (id, report_type, f_user_id)
);

CREATE PROCEDURE report
(
    @f_reported_id INTEGER,
    @report_type ENUM ('PHOTO', 'VIDEO', 'GROUP') NOT NULL,
    @f_user_id INTEGER,
    @statement_type nvarchar(20) = ''  
)
AS
BEGIN
IF @statement_type = 'INSERT'
BEGIN
    insert into Reports (id, report_type, f_reported_id, created_at, updated_at, is_deleted) values( @id, @report_type, @f_reported_id, NOW(), NOW(), 'false')
END
END