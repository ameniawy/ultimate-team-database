CREATE TABLE PendingReports (
    id INTEGER NOT NULL,
    report_status ENUM('APPROVED', 'DECLINED'),
    report_type ENUM ('PHOTO', 'VIDEO', 'GROUP') NOT NULL,
    f_reported_id INTEGER NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    is_deleted BOOLEAN,
    CONSTRAINT p_pending_report PRIMARY KEY (f_reported_id, report_type)
);