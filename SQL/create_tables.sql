


CREATE TABLE blocked (
    f_user_id NUMERIC REFERENCES users(id),
    blocked_user_id NUMERIC REFERENCES users(id),
    CONSTRAINT blocked_pk PRIMARY KEY (user_id, blocked_user_id)
);

CREATE TYPE report_type AS ENUM ('USER', 'PHOTO', 'VIDEO';)

CREATE TABLE reports (
    id NUMERIC NOT NULL,
    report_type report_type,
    user_id NUMERIC REFERENCES users(id),
    ts TIMESTAMP,
    CONSTRAINT report_pk PRIMARY KEY (id, report_type, user_id)
);

