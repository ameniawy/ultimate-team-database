
CREATE TABLE Blocked (
    blocker_user_id INTEGER REFERENCES users(id),
    blocked_user_id INTEGER REFERENCES users(id),
    created_at TIMESTAMPTZ DEFAULT now(),
    updated_at TIMESTAMPTZ DEFAULT now(),
    is_deleted BOOLEAN DEFAULT FALSE,
    CONSTRAINT blocked_pk PRIMARY KEY (blocker_user_id, blocked_user_id)
);

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
   RETURNS TRIGGER AS $$
       BEGIN
           NEW.updated_at = NOW();
       RETURN NEW;
   END;
   $$ LANGUAGE plpgsql;

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON Blocked
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();