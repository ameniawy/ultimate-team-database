
CREATE TYPE gender AS ENUM ('male', 'female');

CREATE TABLE Users (
   id   SERIAL       NOT NULL,
   full_name    VARCHAR(1000) NOT NULL,
   email text not null unique,
   username VARCHAR(100) NOT NULL,
   mobile_number VARCHAR(100) NOT NULL,
   gender gender,
   hashed_pwd VARCHAR(255) NOT NULL,
   active_notifications BOOLEAN NOT NULL,
   is_deleted BOOLEAN NOT NULL DEFAULT FALSE,
   created_at TIMESTAMPTZ DEFAULT now(),
   updated_at TIMESTAMPTZ DEFAULT now(),
   CONSTRAINT user_pk PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
   RETURNS TRIGGER AS $$
       BEGIN
           NEW.updated_at = NOW();
       RETURN NEW;
   END;
   $$ LANGUAGE plpgsql;

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON Users
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();