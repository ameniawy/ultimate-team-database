    CREATE TABLE CurrentlyTrending(
        id SERIAL PRIMARY KEY,
        index INTEGER NOT NUll,
        f_post_id INTEGER REFERENCES Posts(id),
        is_deleted BOOLEAN NOT NULL,
        created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
        updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW() 
    );

